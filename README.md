## Creating a new folder

Representing directory file structure

```
project
│   README.md
│
└───.devcontainer
│   │   devcontainer.json
└───dev
    │   .env
    │   docker-compose.yml
```

Content in .env

```
COMPOSE_PROJECT_NAME=netcore-dev
VERSION=v1.0.0
VERSION_SDK=v3.1
APP=netcore-dev-app
```

Content in docker-compose.yml

```
version: "3.7"

services:
    snp-netcore_sdk3:
        image: hungdlv/snp-netcore_sdk:${VERSION_SDK}
        working_dir: /app
        volumes:
            - "..:/app"
        command: sleep infinity
```

Content in devcontainer.json

```
{
    "name": "netcore dev container",
    "dockerComposeFile": [
        "../dev/docker-compose.yml"
    ],
    "service": "snp-netcore_sdk",
    "workspaceFolder": "/app",
    "extensions": [
        "ms-dotnettools.csharp",
        "josefpihrt-vscode.roslynator",
        "formulahendry.dotnet-test-explorer",
        "fernandoescolar.vscode-solution-explorer",
		"eamodio.gitlens",
		"ms-vscode.powershell",
		"shd101wyy.markdown-preview-enhanced"
    ],
    "shutdownAction": "stopCompose"
}
```

## happy code
